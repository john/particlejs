"use strict";

function particleControler(canvas) {
	this.canvas = document.getElementById(canvas);
	this.maxParticleCount = 100000;
	this.particles = this.getParticleArray();
	this.linearForceX = 0.0;
	this.linearForceY = 0.0;
	this.wallDamping = 1.0;
	this.running = false;
	this.afterGlow = 'off';
	this.spawnCount = 5000;
	this.spawnColor = 0;
	this.QubicForces = true;
	this.resolution = 3;
	this.drawingMethod = true;
	this.interParticleForce = 0;
	this.maxSpeed = 20;

	this.delta = 0;
	this.last_frame_time_ms = false;

	this.lastIndex = 0;
	this.particleCount = 0;
	this.firstFree = 0;

	this.friction = 0.00;

	this.pointerForce = -1000;
	this.pointerForcePosition = false;

	this.init();

	var t = this;

	this.canvas.onmousedown = function(e) {
			var fx = e.pageX * (e.target.width / e.target.offsetWidth);
			var fy = e.pageY * (e.target.height / e.target.offsetHeight);
			t.pointerForcePosition = [fx, fy];
	}
	this.canvas.onmousemove = function(e) {
		if (t.pointerForcePosition) {
			var fx = e.pageX * (e.target.width / e.target.offsetWidth);
			var fy = e.pageY * (e.target.height / e.target.offsetHeight);
			t.pointerForcePosition[0] = fx;
			t.pointerForcePosition[1] = fy;
		}
	}
	this.canvas.onmouseup = function(e) {
		t.pointerForcePosition = false;
	}
}

particleControler.prototype.setResolution = function(g) {
	this.stopSimulation();
	switch (g) {
		case '1':
			this.canvas.height = 480;
			this.canvas.width = 640;
		break;
		case '2':
			this.canvas.height = 600;
			this.canvas.width = 800;
		break;
		case '3':
			this.canvas.height = 768;
			this.canvas.width = 1024;
		break;
		case '4':
			this.canvas.height = 768;
			this.canvas.width = 1366;
		break;
		case '5':
			this.canvas.height = 1080;
			this.canvas.width = 1920;
		break;
	}
	this.init();
	this.startSimulation();
}

particleControler.prototype.setDrawMethod = function(g) {
	if (g == '0') {
		this.drawingMethod = false;
	} else {
		this.drawingMethod = true;
	}
}

particleControler.prototype.startSimulation = function() {
	if (!this.running) {
		this.running = true;
		window.requestAnimationFrame(step);
	}
}

particleControler.prototype.stopSimulation = function() {
	if (this.running) {
		this.running = false;
	}
}

function step(timestamp) {
	pc.loop(timestamp);
	if (pc.running) {
		window.requestAnimationFrame(step);
	}
}

particleControler.prototype.loop = function(timestamp) {
	if (!this.last_frame_time_ms) {
		this.last_frame_time_ms = timestamp;
	}
	var timestep = 1000 / 60;
	this.delta += timestamp - this.last_frame_time_ms;
	this.last_frame_time_ms = timestamp;

	if (this.delta > timestep * 5) {
		this.delta = timestep * 5; //limit delta to prevent glitches
	}

	while (this.delta >= timestep) {
		this.applyForces();
		this.move();
		this.delta -= timestep;
	}
	stats.end();
	stats.begin();
	this.draw();
}

particleControler.prototype.applyForces = function() {
	var arr = this.particles;
	var li = this.lastIndex;

	if (this.pointerForcePosition && this.pointerForce != 0) {
		for (var i = 0; i < li; i += 5) {
			_applyForce(arr, i, this.pointerForcePosition[0], this.pointerForcePosition[1], this.pointerForce, this.QubicForces)
		};
	}
	if (this.interParticleForce != 0) {
		for (var i = 0; i < li; i += 5) {
			for (var j = 0; j < li; j += 5) {
				if (i != j) {
					_applyForce(arr, i, arr[j], arr[j + 1], this.interParticleForce, this.QubicForces)
				}
			}
		}
	}
	if (this.linearForceX != 0 || this.linearForceY != 0) {
		for (var i = 0; i <= li; i += 5) {
			arr[i + 2] += this.linearForceX;
			arr[i + 3] += this.linearForceY;
		};
	};

}

function _applyForce(arr, particle, fx, fy, attraction, qubic) {
	var dist;
	var d = Math.pow(arr[particle + 0] - fx, 2) + Math.pow(arr[particle + 1] - fy, 2)
	var vlength = Math.sqrt(d);

	if (!qubic) {
		dist = vlength;
	} else {
		dist = d;
	}
	if(dist < 300) dist = 300;
	arr[particle + 2] += (arr[particle + 0] - fx) / vlength * (attraction / dist);
	arr[particle + 3] += (arr[particle + 1] - fy) / vlength * (attraction / dist);
}

particleControler.prototype.move = function() {
	var arr = this.particles;
	var boundX = this.boundX;
	var boundY = this.boundY;
	var friction = 1 - this.friction;
	var li = this.lastIndex;
	var ipx, ipy, imx, imy, vlength;

	for (var i = 0; i <= li; i += 5) {
		ipx = i;
		ipy = i + 1;
		imx = i + 2;
		imy = i + 3;

		if (friction != 1) {
			arr[imx] *= friction;
			arr[imy] *= friction;
		}

		// limit particle speed
		vlength = Math.sqrt(Math.pow(arr[imx], 2) + Math.pow(arr[imy], 2));
		if (vlength > this.maxSpeed) {
			arr[imx] = arr[imx] / vlength * this.maxSpeed;
			arr[imy] = arr[imy] / vlength * this.maxSpeed;
		}

		arr[ipx] += arr[imx];
		arr[ipy] += arr[imy];
	}

	if (this.wallDamping < 0) { //translate particle on wall collision
		for (var i = 0; i <= li; i += 5) {
			ipx = i;
			ipy = i + 1;

			if (arr[ipx] < 0) {
				arr[ipx] = this.boundX;
			} else if (arr[ipx] > boundX) {
				arr[ipx] = 0;
			}
			if (arr[ipy] < 0) {
				arr[ipy] = boundY;
			} else if (arr[ipy] > boundY) {
				arr[ipy] = 0;
			}
		};
	} else if (this.wallDamping == 0) { //stop particle on wall collision
		for (var i = 0; i <= li; i += 5) {
			ipx = i;
			ipy = i + 1;
			imx = i + 2;
			imy = i + 3;

			if (arr[ipx] < 0) {
				arr[ipx] = 0;
				arr[imx] = 0;
				arr[imy] = 0;
			} else if (arr[ipx] > boundX) {
				arr[ipx] = boundX;
				arr[imx] = 0;
				arr[imy] = 0;
			}
			if (arr[ipy] < 0) {
				arr[ipy] = 0;
				arr[imx] = 0;
				arr[imy] = 0;
			} else if (arr[ipy] > boundY) {
				arr[ipy] = boundY;
				arr[imx] = 0;
				arr[imy] = 0;
			}
		};
	} else if (this.wallDamping == 1) { //reflect particle on wall collision
		for (var i = 0; i <= li; i += 5) {
			ipx = i;
			ipy = i + 1;
			imx = i + 2;
			imy = i + 3;

			if (arr[ipx] < 0) {
				arr[ipx] = -arr[ipx];
				arr[imx] = -arr[imx];
			} else if (arr[ipx] > boundX) {
				arr[ipx] = boundX - (arr[ipx] - boundX);
				arr[imx] = -arr[imx];
			}
			if (arr[ipy] < 0) {
				arr[ipy] = -arr[ipy];
				arr[imy] = -arr[imy];
			} else if (arr[ipy] > boundY) {
				arr[ipy] = boundY - (arr[ipy] - boundY);
				arr[imy] = -arr[imy];
			}
		};
	} else { // reflect and slow down particle on wall collision
		var damping = this.wallDamping;
		for (var i = 0; i <= li; i += 5) {
			ipx = i;
			ipy = i + 1;
			imx = i + 2;
			imy = i + 3;

			if (arr[ipx] < 0) {
				arr[ipx] = -arr[ipx] * damping;
				arr[imx] = -arr[imx] * damping;
			} else if (arr[ipx] > boundX) {
				arr[ipx] = boundX - ((arr[ipx] - boundX) * damping);
				arr[imx] = -arr[imx] * damping;
			}
			if (arr[ipy] < 0) {
				arr[ipy] = -arr[ipy] * damping;
				arr[imy] = -arr[imy] * damping;
			} else if (arr[ipy] > boundY) {
				arr[ipy] = boundY - ((arr[ipy] - boundY) * damping);
				arr[imy] = -arr[imy] * damping;
			}
		};
	}
}

particleControler.prototype.clear = function() {
	this.stopSimulation();
	this.particles = this.getParticleArray();
	this.lastIndex = 0;
	this.particleCount = 0;
	this.firstFree = 0;
}

particleControler.prototype.draw = function() {
	var b = this.buffer32;
	var w = this.width;
	var h = this.height;
	var li = this.lastIndex;
	var px, py, color, index;

	for (var j = 0; j < li; j += 5) {
		px = ~~(this.particles[j]);
		py = ~~(this.particles[j + 1]);
		color = (this.particles[j + 4]);
		index = (px + (py * w));

		if (this.drawingMethod) {
			if (color == 0) {
				b[index] = 0xff0000ff;
			} else if (color == 1) {
				b[index] = 0xff00ff00;
			} else if (color == 2) {
				b[index] = 0xffff0000;
			}
		} else {
			if (color == 0) {
				b[index] |= 0xff0000ff;
			} else if (color == 1) {
				b[index] |= 0xff00ff00;
			} else if (color == 2) {
				b[index] |= 0xffff0000;
			}
		}
	};

	this.c.putImageData(this.imageData, 0, 0);
		if (this.afterGlow == 'off') {
			for (var i = 0; i < this.buffer32.length; i++) {
				if (this.buffer32[i] != 0) {
					this.buffer32[i] = 0;
				}
			}
		} else if (this.afterGlow == 'bit_shift') {
			for (var i = 0; i < this.buffer32.length; i++) {
				if (this.buffer32[i] != 0) {
					this.buffer32[i] = (this.buffer32[i] & ~(0x01010101)) >> 1;
				}
			}
		} else if (this.afterGlow == 'decrement') {
			for (var i = 0; i < this.buffer8.length; i++) {
				if (this.buffer8[i] != 0) {
					this.buffer8[i] -= 10;
				}
			}
		}
}

particleControler.prototype.addParticle = function(px, py, mx, my, color) {
	var arr = this.particles;
	for (var i = this.firstFree; i < arr.length; i += 5) {
		if (arr[i] == -1) {
			arr[i + 0] = px;
			arr[i + 1] = py;
			arr[i + 2] = mx;
			arr[i + 3] = my;
			arr[i + 4] = color;
			this.particleCount++;
			this.lastIndex = this.lastIndex > i ? this.lastIndex : i;
			this.firstFree = i;
			break;
		}
	};
}

particleControler.prototype.getParticleArray = function() {
	var arr = new Float64Array(5 * this.maxParticleCount);
	for (var i = 0; i < arr.length; i += 5) {
		arr[i] = -1;
	};
	return arr;
}

particleControler.prototype.spawnRandom = function() {
	for (var i = 0; i < this.spawnCount; i++) {
		var px = Math.random()*this.boundX;
		var py = Math.random()*this.boundY;
		this.addParticle(px, py, 0, 0, this.spawnColor);
	};
}

particleControler.prototype.spawnRaster = function() {
	var area = this.boundX * this.boundY;
	var spacing = Math.sqrt(area / this.spawnCount);
	for (var y = spacing / 2; y < this.boundY; y += spacing) {
		for (var x = spacing / 2; x < this.boundX; x += spacing) {
			this.addParticle(x, y, 0, 0, this.spawnColor);
		};
	};
}

particleControler.prototype.init = function() {
	this.c = this.canvas.getContext("2d");

	this.height = this.canvas.height;
	this.width = this.canvas.width;

	this.boundX = this.width -1;
	this.boundY = this.height -1;

	this.imageData = this.c.createImageData(this.width, this.height);
	this.buffer32 = new Uint32Array(this.imageData.data.buffer);
	this.buffer8 = new Uint8ClampedArray(this.imageData.data.buffer);
}
